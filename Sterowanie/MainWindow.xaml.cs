﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OpenTK.Input;
using OpenTK.Graphics;


namespace Sterowanie
{ 
    
    public partial class MainWindow : Window
    {
        double X = 0.0, Y = 0.0;
        double wsp_przes;

        //JoystickState state; //Odczytywanie czy stan Joysticka się zmienił

        double x_start;
        double y_start;

        double wsp_przesX;
        double wsp_przesY;

        double warunek_min;
        double warunek_max;

        int zmiana;
        Thread watek;
        public MainWindow()
        {
            InitializeComponent();
            watek = new Thread(new ThreadStart(update)); //inicjujemy wątek
        }
        

        private void disconnect_Click(object sender, RoutedEventArgs e)
        {
            watek.Abort();  // Zamykamy wątek
            Close();    // Kończymy program
        }
        public delegate void W();
        private void connect_Click(object sender, RoutedEventArgs e)
        {
            var joy = Joystick.GetCapabilities(0); // Joystick o ID = 0
           
            if (connect.IsEnabled && joy.IsConnected) // Sprawdzamy czy Joystick jest podłączony 
            {

                var state = Joystick.GetState(0);
                Console.WriteLine("Joystick 0 podlączony");
                Console.WriteLine(joy.AxisCount);
                Console.WriteLine(joy.ButtonCount);
                Info.Text = "Podlączono";
                Info1.Text = "Liczba przycisków: "+ joy.ButtonCount.ToString();
                Info2.Text = "Liczba kierunków: "+ joy.AxisCount.ToString();

                x_start = state.GetAxis(JoystickAxis.Axis0);
                y_start = state.GetAxis(JoystickAxis.Axis1);
                warunek_min = x_start - 0.2;
                warunek_max = x_start + 0.2;

                watek.Start();  // Startujemy wątek
                connect.IsEnabled = false;
               
            }
            else
                Info.Text = "Nie podłączono";

        }
        
        private  void pozycja()
        {
            if (!this.posX.Dispatcher.CheckAccess())
            {
                Dispatcher.Invoke(new W(pozycja), null);
            }
            else
            {
                if (zmiana != czulosc.SelectedIndex) // Sprawdza wybrany przez użytkownika poziom 
                {

                    if (czulosc.SelectedIndex == 0)
                    {
                        wsp_przes = 0.001;
                    }
                    else if (czulosc.SelectedIndex == 1)
                    {
                        wsp_przes = 0.005;
                    }
                    else if (czulosc.SelectedIndex == 2)
                    {
                        wsp_przes = 0.01;
                    }
                }

                zmiana = czulosc.SelectedIndex; // Zapisywanie wybranego poziomu

                var state = Joystick.GetState(0);

                //Wyświetlanie stanu Joysticka
                posX.Text = Math.Round(state.GetAxis(JoystickAxis.Axis0), 5).ToString();
                posY.Text = Math.Round(state.GetAxis(JoystickAxis.Axis1), 5).ToString();


                //if (state.GetAxis(JoystickAxis.Axis0) > 0.0)
                //{
                //    wsp_przesX = 0.001;
                //}
                //else if (state.GetAxis(JoystickAxis.Axis0) > 0.4)
                //{
                //    wsp_przesX = 0.005;
                //}
                //else if (state.GetAxis(JoystickAxis.Axis0) > 0.7)
                //{
                //    wsp_przesX = 0.01;
                //}

                //if (state.GetAxis(JoystickAxis.Axis1) > 0)
                //{
                //    wsp_przesY = 0.001;
                //}
                //else if (state.GetAxis(JoystickAxis.Axis1) > 0.4)
                //{
                //    wsp_przesY = 0.005;
                //}
                //else if (state.GetAxis(JoystickAxis.Axis1) > 0.7)
                //{
                //    wsp_przesY = 0.01;
                //}

                if (state.GetAxis(JoystickAxis.Axis0) > warunek_max && state.GetAxis(JoystickAxis.Axis0) < warunek_min)
                {
                    if (Math.Round(state.GetAxis(JoystickAxis.Axis0), 4) > x_start)
                        X = X + wsp_przes;
                    else if (Math.Round(state.GetAxis(JoystickAxis.Axis0), 4) < x_start)
                        X = X - wsp_przes;
                }
                if (state.GetAxis(JoystickAxis.Axis1) > warunek_max && state.GetAxis(JoystickAxis.Axis1) < warunek_min)
                {
                    if (Math.Round(state.GetAxis(JoystickAxis.Axis1), 4) > y_start)
                        Y = Y + wsp_przes;
                    else if (Math.Round(state.GetAxis(JoystickAxis.Axis1), 4) < y_start)
                        Y = Y - wsp_przes;
                }
                if (state.GetAxis(JoystickAxis.Axis0) > warunek_max || state.GetAxis(JoystickAxis.Axis0) < warunek_min)
                {   //Sprawdzanie stanów Joysticka i przesuwanie naszej pozycji
                    if ((Math.Round(state.GetAxis(JoystickAxis.Axis0), 4) > x_start) && (Math.Round(state.GetAxis(JoystickAxis.Axis1), 4) > y_start))
                    {
                        X = X + wsp_przes;
                        Y = Y + wsp_przes;
                    }
                    else if ((Math.Round(state.GetAxis(JoystickAxis.Axis0), 4) > x_start) && (Math.Round(state.GetAxis(JoystickAxis.Axis1), 4) < y_start))
                    {
                        X = X + wsp_przes;
                        Y = Y - wsp_przes;
                    }
                    else if ((Math.Round(state.GetAxis(JoystickAxis.Axis0), 4) < x_start) && (Math.Round(state.GetAxis(JoystickAxis.Axis1), 4) > y_start))
                    {
                        X = X - wsp_przes;
                        Y = Y + wsp_przes;
                    }
                    else if ((Math.Round(state.GetAxis(JoystickAxis.Axis0), 4) < x_start) && (Math.Round(state.GetAxis(JoystickAxis.Axis1), 4) < y_start))
                    {
                        X = X - wsp_przes;
                        Y = Y - wsp_przes;
                    }
                }

                    //Zaokrąglanie do 3 miejsc po przecinku naszej pozycji
                    X = Math.Round(X, 4);
                    Y = Math.Round(Y, 4);

                //Wyswietlanie danych
                wspolczynnik.Text = wsp_przes.ToString();
                pozycjaX.Text = X.ToString();
                pozycjaY.Text = Y.ToString();


            }
            
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var state = Joystick.GetState(0); //Odczytywanie czy stan Joysticka się zmienił

            Console.WriteLine("X=" + state.GetAxis(JoystickAxis.Axis0));
            Console.WriteLine("Y=" + state.GetAxis(JoystickAxis.Axis1));
            Console.WriteLine("WSPL_X " + wsp_przesX);
            Console.WriteLine("PosX=" + X.ToString());
            Console.WriteLine("PosY=" + Y.ToString());
        }

        //Aktualizowanie textboxów
        private void update()
        {
            while (true)
            {
                pozycja();
            }

        }

    }
}
